import { useEffect, useRef } from 'react';
import './App.css';

function App() {

  const bg = useRef(null);

useEffect(() => {
  const windowWidth = window.innerWidth / 10;
  const windowHeight = window.innerHeight / 10 ;
  
  bg.current.addEventListener('mousemove', (e) => {
    const mouseX = e.clientX / windowWidth;
    const mouseY = e.clientY / windowHeight;
    
    bg.current.style.transform = `translate3d(-${mouseX}%, -${mouseY}%, 0)`;
  });
}, [])

  return (
    <div ref={bg} style={{height: '100vh', width: '100%'}}>
        <div style={{
          width: '110%',
          height: '110%',
          left: '0',
          top: '0',
          position: 'absolute',
          background: `url('./1145469.jpg') no-repeat left center / cover`
        }} />
    </div>
  );
}

export default App;
